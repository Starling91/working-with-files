<?php
require 'Import.cls.php';
$filePath = $_FILES['file']['tmp_name'];
$file = $_FILES['file']['name'];
$table = $_POST['table'];

$post = new ImportData();
switch (strrchr($file,'.')){
    case '.csv': $post->fromCSV($table,$filePath);
        break;
    case '.json': $post->fromJSON($table,$filePath);
        break;
    case '.xml': $post->fromXML($table, $filePath);
        break;
}
