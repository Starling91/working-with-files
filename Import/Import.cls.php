<?php
class ImportData {

    private function Insert($table, $column, $cell, $pair)
    {
        try {
            require 'dbConnect.php';
            $sql = "INSERT INTO $table ($column) VALUES($cell) ON DUPLICATE KEY UPDATE $pair";
            $result = $pdo->exec($sql);
            if ($result !==0){
                require_once 'succesfull.html';
            } else require_once'allredy.html';
        }
        catch(Exception $e){
            require_once 'error.html';
            exit();
        }
    }

    public function fromCSV($table,$filePath)
    {
        require 'dbConnect.php';
        $csvFile = fopen($filePath, "r");
        if ($csvFile) {
            $data = array();
            while (($csvData = fgetcsv($csvFile, 1000, ",")) !== FALSE) {
                $data[] = $csvData;
            }
            $columnAr=array_shift($data);
            $result=array();
            for($i=0;$i<count($data);$i++){
                $result[]=array_combine($columnAr,$data[$i]);
            }
            foreach($result as $v){
                $pairAr = array();
                foreach($v as $k=>$v2) {
                    $pairAr[] = $k . '=' . '"' . $v2 . '"';
                }
                $pair = (implode(',',$pairAr));
                $column = (implode(',',$columnAr));
                $cell = ('"'.implode('","',$v).'"');
                $this->Insert($table,$column,$cell,$pair);
            }
        }
    }

    public function fromJSON($table,$filePath)
    {
        $jsonFile = file_get_contents($filePath);
        $jsonArray = json_decode($jsonFile, 1);
        foreach($jsonArray as $k=>$v){
            $pairAr = array();
            foreach ($v as $key=>$val){
                $pairAr[] = $key.'='.'"'.$val.'"';
            }
            $ar = (array_keys($v));
            $column = (implode(',',$ar));
            $cell = ('"'.implode('","',$v).'"');
            $pair = (implode(',',$pairAr));
            $this->Insert($table,$column,$cell,$pair);
        }
    }

    public function fromXML($table, $filePath)
    {
        require 'dbConnect.php';
        $xml = simplexml_load_file($filePath);
        foreach($xml as $val){
            $toArray = (array)$val;
            $pairAr = array();
            foreach ($toArray as $key=>$v){
                $pairAr[] = $key.'='.'"'.$v.'"';
            }
            $ar = (array_keys($toArray));
            $column = (implode(',',$ar));
            $cell = ('"'.implode('","',$toArray).'"');
            $pair = (implode(',',$pairAr));
            $this->Insert($table,$column,$cell,$pair);
        }
    }
}