<?php
require 'ConvertData.Cls.php';
$table = $_POST['table'];
$format = $_POST['format'];

$post = new ConvertData();
switch ($format){
    case "CSV":$post->toCSV($table);
        break;
    case "JSON":$post->toJSON($table);
        break;
    case "XML":$post->toXML($table);
        break;
}