<?php
class ConvertData {

    public  function toCSV($table){
        require 'dbConnect.php';
        try {
            $sql = "SELECT * FROM $table";
            $result = $pdo->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            if (count($rows)) {
                $fp = fopen('files/'.$table.'.csv', 'w');
                $str=array();
                foreach ($rows as $fields) {
                    $ar = (array_keys($fields));
                    $str[] =$fields;
                }
                fputcsv($fp,$ar);
                foreach($str as $val){
                    fputcsv($fp,$val);
                }
                fclose($fp);
                $filePath = 'files/'.$table.'.csv';
                echo  $filePath;
            } else {
                echo "<h2>Таблица пустая!</h2>";
            }
        }
        catch (Exception $e){
            echo "Ошибка при извлечении данных!". $e->getMessage();
            exit();
        }

    }

    public  function toJSON($table){
        require 'dbConnect.php';
        try {
            $sql = "SELECT * FROM $table";
            $result = $pdo->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            if (count($rows)) {
                $fp = fopen('files/'.$table.'.json', 'w');

                $jsonStr = json_encode($rows, JSON_UNESCAPED_UNICODE);
                if(fwrite($fp, $jsonStr));

                fclose($fp);
                $filePath = 'files/'.$table.'.json';
                echo  $filePath;
            } else {
                echo "<h2>Таблица пустая!</h2>";
            }
        }
        catch (Exception $e){
            echo "Ошибка при извлечении данных!". $e->getMessage();
            exit();
        }
    }

    public  function toXML($table){
        require 'dbConnect.php';
        try {
            $sql = "SELECT * FROM $table";
            $result = $pdo->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            if (count($rows)) {
                $dom = new DOMDocument('1.0', 'UTF-8');
                $tName = $dom->createElement($table);
                $dom->appendChild($tName);
                foreach($rows as $k=>$v) {
                    $row = $dom->createElement('row');
                    $tName->appendChild($row);
                    foreach($v as $key=>$val) {
                        $column = $dom->createElement("$key", $val);
                        $row->appendChild($column);
                    }
                }
                $dom->save('files/'.$table.'.xml');
                $filePath = 'files/'.$table.'.xml';
                echo  $filePath;

            } else {
                echo "<h2>Таблица пустая!</h2>";
            }
        } catch (Exception $e) {
            echo "Ошибка при извлечении данных!" . $e->getMessage();
            exit();
        }
    }
}